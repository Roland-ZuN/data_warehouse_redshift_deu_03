# -*- coding: utf-8 -*-
"""Script to perform ETL process.

This process copy the JSON files content into the staging tables, then
parsed the staging data content to the fact and dimension tables.

Mantaniner: Rolando Gonzalez
Version: 1.0.0
"""
# Standard library imports
# None

# Third-party imports
import pandas as pd

# Propietary imports
import database_handler
import sql_queries

def load_staging_tables(db_handler, verbose=False):
    """Copy the JSON data from S3 bucket to the staging tables

    The function uses the queries in 'copy_table_queries' list, the list
    is located in the 'sql_queries.py' file.

    Parameters
    ----------
    db_handler : Instance
        description -> psycopg2 instance database handler
        format -> No apply
        options -> No apply

    verbose : bool
        description -> Print process workflow or results, useful for
            debugging
        format -> No apply
        options -> No apply
    """
    queries = sql_queries.Queries.generate_copy_queries(
        s3_info=db_handler.s3_info,
        iam_role_info=db_handler.iam_role_info
    )
    print("\nCopying JSON files to staging tables...\n")
    for query in queries:
        if verbose:
            print(f"{query}")
        db_handler.cursor.execute(query)

def insert_tables(cursor, verbose=False):
    """Copy the JSON data from S3 bucket to the staging tables

    The function uses the queries in 'copy_table_queries' list, the list
    is located in the 'sql_queries.py' file.

    Parameters
    ----------
    cursor : Instance
        description -> psycopg2 instance database instance cursor
        format -> No apply
        options -> No apply

    verbose : bool
        description -> Print process workflow or results, useful for
            debugging
        format -> No apply
        options -> No apply
    """
    queries = sql_queries.Queries()
    for query in queries.insert_table_queries:
        if verbose:
            print(f"{query}")
        cursor.execute(query)

def main():
    """ Pipeline execution

    1. Establish connection to a Redshift database and get the
       connection and cursor instances
    2. Copy JSON files contents from a S3 bucket to the staging tables
    3. Parse the staging data into the fact and dimension tables
    4. Execute the designed metric queries.
    5. Close the connection.
    """
    # Setup variables
    config_file_name = 'dwh.cfg'
    verbose = True

    # Pipeline steps.
    # 1: Open connection
    db_handler = database_handler.Connection(config_file_path=config_file_name)
    db_handler.open_connection(verbose=verbose)

    # 2: Copy data
    load_staging_tables(db_handler, verbose=verbose)

    # 3: Parse data
    insert_tables(db_handler.cursor, verbose=verbose)

    # 4: Get some metrics from designed metric queries
    levels_df = pd.read_sql(
        sql_queries.Queries.level_users_query(verbose=True),
        db_handler.connection
    )
    print("Level results:\n")
    print(levels_df.head())

    frequency_df = pd.read_sql(
        sql_queries.Queries.frequent_hours_query(rows=5, verbose=True),
        db_handler.connection
    )
    print("Frequency results:\n")
    print(frequency_df.head())

    # 5: Close connection
    db_handler.close_connection(verbose=verbose)

if __name__ == "__main__":
    # Execute pipeline
    main()

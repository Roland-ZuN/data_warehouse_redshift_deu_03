# -*- coding: utf-8 -*-
"""SQL Queries used in the development.

The current script has all the statements to create TABLES and INSERT
data into them, also this includes DROP statements and other queries to
retrieve information and insights.

The current SQL statements are based on Redshift.

Mantaniner: Rolando Gonzalez
Version: 1.0.0
"""
# Standard library imports
# None

# Third-party imports
# None

# Propietary imports
# None

class Queries():
    """Queries class.

    SQL statements to use for creation, drop, retrieving, manipulating
    and storing data; the queries are designed to be use only with the
    Redshift DBMS.

    Attributes
    ----------
    create_table_queries : String List
        description -> Each element has a query to create an especific
            table, the creation order is:

                1. 'staging_events_table_create'
                2. 'staging_songs_table_create'
                3. 'time_table_create'
                4. 'user_table_create'
                5. 'artist_table_create'
                6. 'song_table_create'
                7. 'songplay_table_create'

            Create tables in this order allows to reference the primary
            keys from tables with an especific foreign key.
        format -> No apply
        options -> No apply

    drop_table_queries : String List
        description -> Each element has a query to drop an especific
            table, the deletion order is:

                1. 'staging_events_table_drop'
                2. 'staging_songs_table_drop'
                3. 'songplay_table_drop'
                4. 'time_table_drop'
                5. 'user_table_drop'
                6. 'song_table_drop'
                7. 'artist_table_drop'

        format -> No apply
        options -> No apply

    insert_table_queries : String List
    description -> Each element has a query to insert and update
        specific fact and dimension tables, the query order and the new
        queries created help to avoid duplicated values in main IDs like
        user_id, song_id and artist_id, the queries order are:

            1. 'time_table_insert'
            2. 'user_table_insert'
            3. 'user_table_update'
            4. 'artist_table_insert'
            5. 'artist_table_update'
            6. 'song_table_insert'
            7. 'song_table_update'
            8. 'songplay_table_insert'

    format -> No apply
    options -> No apply

    Methods
    -------
    generate_copy_queries : Static Method
        Method to create a COPY query, this query will copy the JSON
        files stored in the S3 Bucket to the staging tables.

    song_select : Static Method
        Method to create a retrieve query statement from a dataframe
        with artists name, title songs and duration of the song. The
        tables used are 'songs' and 'artists'.
    """
    def __init__(self):
        # Create staging table queries
        staging_events_table_create = (
            "CREATE TABLE IF NOT EXISTS staging_events\n"
            "(\n"
            "    artist_name varchar(256),\n"
            "    auth varchar(32),\n"
            "    user_first_name varchar(256),\n"
            "    user_gender char(1),\n"
            "    item_session smallint,\n"
            "    user_last_name varchar(256),\n"
            "    song_duration numeric(9, 5),\n"
            "    user_level varchar(32),\n"
            "    event_location varchar(512),\n"
            "    event_method varchar(8),\n"
            "    event_page varchar(64),\n"
            "    event_register timestamp,\n"
            "    session_id int,\n"
            "    song_title varchar(256),\n"
            "    status smallint,\n"
            "    timestamp \"timestamp\" NOT NULL SORTKEY,\n"
            "    user_agent varchar(10000),\n"
            "    user_id int\n"
            ") DISTSTYLE EVEN;\n"
        )

        staging_songs_table_create = (
            "CREATE TABLE IF NOT EXISTS staging_songs\n"
            "(\n"
            "    artist_id char(18),\n"
            "    artist_latitude numeric(7, 5),\n"
            "    artist_location varchar(512),\n"
            "    artist_longitude numeric(8, 5),\n"
            "    artist_name varchar(256),\n"
            "    duration numeric(9, 5),\n"
            "    num_songs smallint,\n"
            "    song_id char(18),\n"
            "    title varchar(256) SORTKEY,\n"
            "    year smallint\n"
            ") DISTSTYLE ALL;\n"
        )

        # Create dimension table queries
        time_table_create = (
            "CREATE TABLE IF NOT EXISTS \"time\"\n"
            "(\n"
            "    start_time timestamp PRIMARY KEY SORTKEY,\n"
            "    hour smallint NOT NULL,\n"
            "    day smallint NOT NULL,\n"
            "    week smallint NOT NULL,\n"
            "    month smallint NOT NULL,\n"
            "    year smallint NOT NULL,\n"
            "    weekday smallint NOT NULL\n"
            ") DISTSTYLE ALL;\n"
        )

        user_table_create = (
            "CREATE TABLE IF NOT EXISTS \"user\"\n"
            "(\n"
            "    user_id int PRIMARY KEY SORTKEY,\n"
            "    first_name varchar(256),\n"
            "    last_name varchar(256),\n"
            "    gender char(1),\n"
            "    level varchar(32)\n"
            ") DISTSTYLE ALL;\n"
        )

        artist_table_create = (
            "CREATE TABLE IF NOT EXISTS artist\n"
            "(\n"
            "    artist_id char(18) PRIMARY KEY SORTKEY,\n"
            "    latitude numeric(7, 5),\n"
            "    longitude numeric(8, 5),\n"
            "    location varchar(512),\n"
            "    name varchar(256)\n"
            ") DISTSTYLE ALL;\n"
        )

        song_table_create = (
            "CREATE TABLE IF NOT EXISTS song\n"
            "(\n"
            "    song_id char(18) PRIMARY KEY,\n"
            "    title varchar(256) SORTKEY,\n"
            "    year smallint,\n"
            "    artist_id char(18),\n"
            "    duration numeric(9, 5),\n"
            "    FOREIGN KEY (artist_id)\n"
            "    REFERENCES artist (artist_id)\n"
            ") DISTSTYLE ALL;\n"
        )

        # Create fact table query
        songplay_table_create = (
            "CREATE TABLE IF NOT EXISTS songplay\n"
            "(\n"
            "    songplay_id bigint IDENTITY(0, 1) PRIMARY KEY,\n"
            "    start_time timestamp NOT NULL SORTKEY,\n"
            "    user_id int,\n"
            "    level varchar(32),\n"
            "    song_id varchar(18),\n"
            "    artist_id varchar(18),\n"
            "    session_id int,\n"
            "    location varchar(512),\n"
            "    user_agent text,\n"
            "    FOREIGN KEY (start_time)\n"
            "    REFERENCES \"time\" (start_time),\n"
            "    FOREIGN KEY (user_id)\n"
            "    REFERENCES \"user\" (user_id),\n"
            "    FOREIGN KEY (song_id)\n"
            "    REFERENCES song (song_id),\n"
            "    FOREIGN KEY (artist_id)\n"
            "    REFERENCES artist (artist_id)\n"
            ") DISTSTYLE EVEN;\n"
        )

        # Drop table queries
        staging_events_table_drop = ("DROP TABLE IF EXISTS staging_events;\n")
        staging_songs_table_drop = ("DROP TABLE IF EXISTS staging_songs;\n")
        songplay_table_drop = ("DROP TABLE IF EXISTS songplay;\n")
        time_table_drop = ("DROP TABLE IF EXISTS \"time\";\n")
        user_table_drop = ("DROP TABLE IF EXISTS \"user\";\n")
        song_table_drop = ("DROP TABLE IF EXISTS song;\n")
        artist_table_drop = ("DROP TABLE IF EXISTS artist;\n")

        # Insert data queries
        time_table_insert = (
            "INSERT INTO \"time\"\n"
            "SELECT\n"
            "    DISTINCT(se.\"timestamp\") AS start_time,\n"
            "    EXTRACT(HOUR FROM se.\"timestamp\") AS \"hour\",\n"
            "    EXTRACT(DAY FROM se.\"timestamp\") AS \"day\",\n"
            "    EXTRACT(WEEK FROM se.\"timestamp\") AS \"week\",\n"
            "    EXTRACT(MONTH FROM se.\"timestamp\") AS \"month\",\n"
            "    EXTRACT(YEAR FROM se.\"timestamp\") AS \"year\",\n"
            "    EXTRACT(DAYOFWEEK  FROM se.\"timestamp\") AS weekday\n"
            "FROM staging_events AS se\n"
            "WHERE se.\"timestamp\" NOT IN\n"
            "(SELECT DISTINCT(start_time) FROM \"time\");\n"
        )

        # Redshift does not support 'upsert' method or similar unique
        # method to perform this similar task (upsert). To support the
        # user level insertion and update without the 'user_id'
        # duplication issue, it is required to perform two separated
        # queries:
        #
        #    1. Insert the unique user_id from the table 'staging_event'
        #       without the other user data columns into the fact table
        #       'user'.
        #    2. Update the fact table 'user' in the rest of colums
        #       using the last user data occurrence of 'user_id' in
        #       'staging_events' table
        # https://knowledge.udacity.com/questions/444676
        user_table_insert = (
            "INSERT INTO \"user\" (user_id)\n"
            "SELECT DISTINCT(se.user_id) AS user_id\n"
            "FROM staging_events AS se\n"
            "WHERE se.user_id IS NOT NULL\n"
            "AND se.user_id NOT IN (SELECT DISTINCT(user_id) FROM \"user\");\n"
        )

        # This query update the rest of columns in table 'user', using
        # the most recent data columns from the table 'staging_events',
        # it allows to update the user data to the most recent
        # information stored in the 'staging_events', updating the data
        # of all users without duplicated 'user_id's.
        # https://stackoverflow.com/questions/55024148/sql-query-on-redshift-to-get-the-first-and-the-last-value
        user_table_update = (
            "UPDATE \"user\"\n"
            "SET\n"
            "    first_name = lv.user_first_name,\n"
            "    last_name = lv.user_last_name,\n"
            "    gender = lv.user_gender,\n"
            "    \"level\" = lv.user_level\n"
            "FROM\n"
            "(\n"
            "    SELECT\n"
            "        DISTINCT se.user_id,\n"
            "        FIRST_VALUE(se.user_first_name) OVER\n"
            "        (\n"
            "            PARTITION BY se.user_id\n"
            "            ORDER BY se.\"timestamp\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS user_first_name,\n"
            "        FIRST_VALUE(se.user_last_name) OVER\n"
            "        (\n"
            "            PARTITION BY se.user_id\n"
            "            ORDER BY se.\"timestamp\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS user_last_name,\n"
            "        FIRST_VALUE(se.user_gender) OVER\n"
            "        (\n"
            "            PARTITION BY se.user_id\n"
            "            ORDER BY se.\"timestamp\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS user_gender,\n"
            "        FIRST_VALUE(se.user_level) OVER\n"
            "        (\n"
            "            PARTITION BY se.user_id\n"
            "            ORDER BY se.\"timestamp\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS user_level\n"
            "    FROM staging_events AS se\n"
            "    WHERE se.user_id IS NOT NULL\n"
            ") lv\n"
            "WHERE \"user\".user_id = lv.user_id;\n"
        )

        # To insert unique data artists, follow the same process from
        # above (Insert and update), it avoids duplicated 'artists_id's
        artist_table_insert = (
            "INSERT INTO artist (artist_id)\n"
            "SELECT DISTINCT(ss.artist_id) AS artist_id\n"
            "FROM staging_songs AS ss\n"
            "WHERE ss.artist_id IS NOT NULL\n"
            "AND ss.artist_id NOT IN\n"
            "(SELECT DISTINCT(artist_id) FROM artist);\n"
        )

        artist_table_update = (
            "UPDATE artist\n"
            "SET\n"
            "    latitude = ad.artist_latitude,\n"
            "    longitude = ad.artist_longitude,\n"
            "    location = ad.artist_location,\n"
            "    \"name\" = ad.artist_name\n"
            "FROM\n"
            "(\n"
            "    SELECT\n"
            "        DISTINCT ss.artist_id,\n"
            "        FIRST_VALUE(ss.artist_latitude) OVER\n"
            "        (\n"
            "            PARTITION BY ss.artist_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS artist_latitude,\n"
            "        FIRST_VALUE(ss.artist_longitude) OVER\n"
            "        (\n"
            "            PARTITION BY ss.artist_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS artist_longitude,\n"
            "        FIRST_VALUE(ss.artist_location) OVER\n"
            "        (\n"
            "            PARTITION BY ss.artist_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS artist_location,\n"
            "        FIRST_VALUE(ss.artist_name) OVER\n"
            "        (\n"
            "            PARTITION BY ss.artist_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS artist_name\n"
            "    FROM staging_songs AS ss\n"
            "    WHERE ss.artist_id IS NOT NULL\n"
            ") ad\n"
            "WHERE artist.artist_id = ad.artist_id;\n"
        )

        # Insert and update 'song' table
        song_table_insert = (
            "INSERT INTO song (song_id)\n"
            "SELECT DISTINCT(ss.song_id) AS song_id\n"
            "FROM staging_songs AS ss\n"
            "WHERE ss.song_id IS NOT NULL\n"
            "AND ss.song_id NOT IN (SELECT DISTINCT(song_id) FROM song);\n"
        )

        song_table_update = (
            "UPDATE song\n"
            "SET\n"
            "    title = sd.title,\n"
            "    \"year\" = sd.\"year\",\n"
            "    artist_id = sd.artist_id,\n"
            "    duration = sd.duration\n"
            "FROM\n"
            "(\n"
            "    SELECT\n"
            "        DISTINCT ss.song_id,\n"
            "        FIRST_VALUE(ss.title) OVER\n"
            "        (\n"
            "            PARTITION BY ss.song_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS title,\n"
            "        FIRST_VALUE(ss.\"year\") OVER\n"
            "        (\n"
            "            PARTITION BY ss.song_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS \"year\",\n"
            "        FIRST_VALUE(ss.artist_id) OVER\n"
            "        (\n"
            "            PARTITION BY ss.song_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS artist_id,\n"
            "        FIRST_VALUE(ss.duration) OVER\n"
            "        (\n"
            "            PARTITION BY ss.song_id\n"
            "            ORDER BY ss.\"year\" DESC\n"
            "            ROWS BETWEEN UNBOUNDED PRECEDING\n"
            "            AND UNBOUNDED FOLLOWING\n"
            "        ) AS duration\n"
            "    FROM staging_songs AS ss\n"
            "    WHERE ss.song_id IS NOT NULL\n"
            ") sd\n"
            "WHERE song.song_id = sd.song_id;\n"
        )

        # This query allows the insertion of NULL values in the
        # 'song_id' and 'artist_id' attributes, so the LEFT JOIN is
        # performed, it is decided to do it on this way because some
        # songs do not exist in the 'staging_songs' table, but the users
        # are still using the app, so to avoid losing the frequency of
        # use, the insertion allow the NULL values, to avoid including
        # NULL values, just make a INNER JOIN instead a LEFT JOIN.
        #
        # Besides, the LEFT JOIN is preferred because on this way,
        # majority of information is kept, and to get the songplays with
        # no NULL values in 'song_id' and 'artist_id', the developer
        # just have to filter the 'songplay' table using a WHERE
        # statement to retrieve songplays with no NULL values.
        songplay_table_insert = (
            "INSERT INTO songplay\n"
            "(\n"
            "    start_time,\n"
            "    user_id,\n"
            "    \"level\",\n"
            "    song_id,\n"
            "    artist_id,\n"
            "    session_id,\n"
            "    location,\n"
            "    user_agent\n"
            ")\n"
            "SELECT\n"
            "    DISTINCT(bd.\"timestamp\") AS start_time,\n"
            "    bd.user_id,\n"
            "    bd.user_level AS \"level\",\n"
            "    ss.song_id,\n"
            "    ss.artist_id,\n"
            "    bd.session_id,\n"
            "    bd.event_location AS location,\n"
            "    bd.user_agent\n"
            "FROM\n"
            "(\n"
            "    SELECT * FROM staging_events\n"
            "    WHERE event_page = 'NextSong'\n"
            ") AS bd\n"
            "LEFT JOIN staging_songs AS ss\n"
            "ON bd.song_title = ss.title\n"
            "AND bd.artist_name = ss.artist_name\n"
            "AND bd.song_duration = ss.duration;\n"
        )

        # Query lists
        self.create_table_queries = [
            staging_events_table_create,
            staging_songs_table_create,
            time_table_create,
            user_table_create,
            artist_table_create,
            song_table_create,
            songplay_table_create
        ]

        self.drop_table_queries = [
            staging_events_table_drop,
            staging_songs_table_drop,
            songplay_table_drop,
            time_table_drop,
            user_table_drop,
            song_table_drop,
            artist_table_drop
        ]

        self.insert_table_queries = [
            time_table_insert,
            user_table_insert,
            user_table_update,
            artist_table_insert,
            artist_table_update,
            song_table_insert,
            song_table_update,
            songplay_table_insert
        ]

    @staticmethod
    def generate_copy_queries(s3_info, iam_role_info, verbose=False):
        """Method to create the required queries to insert data

        The queries are designed to perform all the insertion process
        avoiding to include duplicated IDs (artist_id, song_id and
        artist_id are uniques).

        On general, it is reached using an additional query, an UPDATE
        query including a FIRST_LINE statament, this allows to UPDATE
        the fact tables only with the most recent values registered by
        user, song and artist.

        Parameters
        ----------
        s3_info : Dictionary
            description -> Data extracted from the 'cfg' file from the
                [S3] field.
            format -> {
                'LOG_DATA': '<s3_host>',
                'LOG_JSONPATH': '<s3_host>',
                'SONG_DATA': '<s3_host>'
            }
            options -> No apply

        iam_role_info : Dictionary
            description -> Data extracted from the 'cfg' file from the
                [IAM_ROLE] field.
            format -> {
                'ARN': '<arn_aws_iam_code_role>'
            }
            options -> No apply

        verbose : bool
            description -> Print process workflow or results, useful for
                debugging
            format -> No apply
            options -> No apply

        Returns
        -------
        query : string
            description -> The complete SQL statement
            format -> No apply
            options -> No apply
        """
        if verbose:
            print(s3_info)
            print(iam_role_info)

        staging_events_copy = (
            "COPY staging_events\n"
            f"FROM '{s3_info['LOG_DATA']}'\n"
            f"IAM_ROLE '{iam_role_info['ARN']}'\n"
            f"JSON '{s3_info['LOG_JSONPATH']}'\n"
            "REGION 'us-west-2'\n"
            "TIMEFORMAT AS 'epochmillisecs';\n"
        )

        staging_songs_copy = (
            "COPY staging_songs\n"
            f"FROM '{s3_info['SONG_DATA']}'\n"
            f"IAM_ROLE '{iam_role_info['ARN']}'\n"
            f"JSON 'auto'\n"
            "REGION 'us-west-2'\n"
            "TIMEFORMAT AS 'epochmillisecs';\n"
        )

        copy_table_queries = [
            staging_events_copy,
            staging_songs_copy
        ]

        return copy_table_queries

    @staticmethod
    def level_users_query(verbose=False):
        """Method to create the query to get the total user levels.

        Parameters
        ----------
        verbose : bool
            description -> Print process workflow or results, useful for
                debugging
            format -> No apply
            options -> No apply

        Returns
        -------
        query : string
            description -> The complete SQL statement
            format -> No apply
            options -> No apply
        """
        query = (
            "SELECT u.\"level\", COUNT(u.\"level\")\n"
            "FROM \"user\" u\n"
            "GROUP BY u.\"level\";\n"
        )

        if verbose:
            print(f"Get total level of users:\n{query}\n")

        return query

    @staticmethod
    def frequent_hours_query(rows=10, verbose=False):
        """Method to create the query to get the hours with most app use

        Parameters
        ----------
        rows : int
            description -> Number of results to query
            format -> No apply
            options -> No apply

        verbose : bool
            description -> Print process workflow or results, useful for
                debugging
            format -> No apply
            options -> No apply

        Returns
        -------
        query : string
            description -> The complete SQL statement
            format -> No apply
            options -> No apply
        """
        query = (
            "SELECT t.\"hour\", COUNT(t.\"hour\") AS frequency\n"
            "FROM songplay sp\n"
            "JOIN \"time\" t\n"
            "ON sp.start_time = t.start_time\n"
            "GROUP BY t.\"hour\"\n"
            "ORDER BY frequency DESC\n"
            f"LIMIT {rows};\n"
        )

        if verbose:
            print(f"Hours where the app is most used:\n{query}\n")

        return query

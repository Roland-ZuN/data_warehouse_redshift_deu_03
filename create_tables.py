# -*- coding: utf-8 -*-
"""Script to create and drop tables.

This process make sure that the tables can be created successfully and
helps to debug queries if needed.

Mantaniner: Rolando Gonzalez
Version: 1.0.0
"""
# Standard library imports
# None

# Third-party imports
# None

# Propietary imports
import database_handler
import sql_queries

def drop_tables(cursor, verbose=False):
    """Drop all the tables if they exist

    The function uses the queries in 'drop_table_queries' list, the list
    is located in the 'sql_queries.py' file.

    Parameters
    ----------
    cursor : Instance
        description -> psycopg2 instance database cursor
        format -> No apply
        options -> No apply

    verbose : bool
        description -> Print process workflow or results, useful for
            debugging
        format -> No apply
        options -> No apply
    """
    queries = sql_queries.Queries()
    print("\nDrop tables...\n")
    for query in queries.drop_table_queries:
        if verbose:
            print(f"{query}")
        cursor.execute(query)

def create_tables(cursor, verbose=False):
    """ Creat the tables if they no exist.

    Each table is created using the queries in the
    `create_table_queries` list, the list is located in the
    'sql_queries.py' file.

    Parameters
    ----------
    cursor : Instance
        description -> psycopg2 instance database cursor
        format -> No apply
        options -> No apply

    verbose : bool
        description -> Print process workflow or results, useful for
            debugging
        format -> No apply
        options -> No apply
    """
    queries = sql_queries.Queries()
    print("\nCreate tables...\n")
    for query in queries.create_table_queries:
        if verbose:
            print(f"{query}")
        cursor.execute(query)

def main():
    """ Pipeline execution

    1. Establish connection to a Redshift database and get the
       connection and cursor instances
    2. Drop all the tables if they exist.
    3. Create all the tables if they no exist.
    4. Close the connection.
    """
    # Setup variables
    config_file_name = 'dwh.cfg'
    verbose = True

    # Pipeline steps.
    # 1: Open connection
    db_handler = database_handler.Connection(config_file_path=config_file_name)
    db_handler.open_connection(verbose=verbose)

    # 2: Deletion
    drop_tables(db_handler.cursor, verbose=verbose)

    # 3: Creation
    create_tables(db_handler.cursor, verbose=verbose)

    # 4: Close connection
    db_handler.close_connection(verbose=verbose)

if __name__ == "__main__":
    # Execute pipeline
    main()

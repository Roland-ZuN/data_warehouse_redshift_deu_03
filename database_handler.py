# -*- coding: utf-8 -*-
"""Database handler.

Contains the necessary methods to make the database connection. The
current code are based oto handle Redshift as DBMS.

Mantaniner: Rolando Gonzalez
Version: 1.0.0
"""
# Standard library imports
import configparser
import os
import traceback

# Third-party imports
import psycopg2

# Propietary imports
# None

class Connection():
    """Conection class.

    Designed to handle Redshift database connection.

    Attributes
    ----------
    config_file_path : String
        description -> Path to the configuration file containing the
            database information, access credentials and roles.
        format -> '<file_name>.cfg'
        options -> The configuration file must have at least three
            sections called CLUSTER, IAM_ROLE, and S3 as follows:

            [CLUSTER]
            HOST=<database_host_path>
            DB_NAME=<database_name>
            DB_USER=<database_user>
            DB_PASSWORD=<database_password>
            DB_PORT=<database_port_number>

            [IAM_ROLE]
            ARN=<arn_aws_iam_code_role>

            [S3]
            LOG_DATA=<s3_host>
            LOG_JSONPATH=<s3_host>
            SONG_DATA=<s3_host>

            NOTE: If a field has a '%' character, it is required to
                  duplicate the character ('%%') to avoid 'configarser'
                  generating an error at parsing

    connection : bool to instance
        description -> The connection instance, the default value is
            'False' if no Database is opened
        format -> No apply
        options -> No apply

    cursor : bool to instance
        description -> The cursor instance, the default value is 'False'
            if no Database is opened
        format -> No apply
        options -> No apply

    config : dictionary
        description -> Database information required for connection
        format -> {
            "HOST": string,
            "DB_NAME": string,
            "DB_USER": string,
            "DB_PASSWORD": string,
            "DB_PORT": int
        }
        options -> No apply

    Methods
    -------
    print_db_info : public method
        Print the database public information

    open_connection : public method
        Open connection to the database

    close_connection : public method
        Close connection to the database
    """
    def __init__(self, config_file_path):
        # TODO: Verify all the fields, include the correct parameters in
        #       the corresponging fields in the sections called AWS,
        #       CLUSTER, and S3 from the 'cfg' file.

        # Load configuration file
        config_file_path = os.path.join(os.getcwd(), config_file_path)
        config = configparser.ConfigParser()
        config.read(config_file_path)

        self.config_file_path = config_file_path
        self.connection = False
        self.cursor = False
        self.db_info = config['CLUSTER']
        self.iam_role_info = config['IAM_ROLE']
        self.s3_info = config['S3']

    def print_db_info(self):
        """Method to print the database information
        """
        print(f"Configuration file path:\n{self.config_file_path}")
        print(f"Host:\n{self.db_info['HOST']}")
        print(f"Database name:\n{self.db_info['DB_NAME']}")
        print(f"Port:\n{self.db_info['DB_PORT']}")
        print(f"Database user:\n{self.db_info['DB_USER']}\n")

    def open_connection(self, verbose=False):
        """Method to open the database connection

        Parameters
        ----------
        verbose : bool
            description -> Print process workflow or results, useful for
                debugging
            format -> No apply
            options -> No apply
        """
        if verbose:
            print("\nDatabase information to open connection.")
            self.print_db_info()

        try:
            self.connection = psycopg2.connect(
                user=self.db_info['DB_USER'],
                password=self.db_info['DB_PASSWORD'],
                host=self.db_info['HOST'],
                port=self.db_info['DB_PORT'],
                dbname=self.db_info['DB_NAME']
            )
            self.connection.set_session(autocommit=True)
            self.cursor = self.connection.cursor()

        except psycopg2.Error as error:
            self.connection = False
            self.cursor = False
            message = traceback.format_exc()
            print("ERROR: Connection to the Redshift database failed!!! :c\n")
            print(f"Error:\n{error}\n")
            print(f"Complete log error:\n{message}\n")

        if self.connection == False or self.connection == False:
            print("\nConnection closed. :c\n")
        if self.connection != False and self.connection != False:
            print("\nConnection opened. c:\n")

    def close_connection(self, verbose=False):
        """Method to close the Database connection

        Parameters
        ----------
        verbose : bool
            description -> Print process workflow or results, useful for
                debugging
            format -> No apply
            options -> No apply
        """
        if verbose:
            print("Database information to close connection.")
            self.print_db_info()

        self.cursor.close()
        self.connection.close()
        self.connection = False
        self.cursor = False

        if self.connection == False or self.connection == False:
            print("\nConnection closed. c:\n")
        if self.connection != False and self.connection != False:
            print("\nConnection close process failed. :c\n")

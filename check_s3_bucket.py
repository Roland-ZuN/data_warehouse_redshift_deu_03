import boto3
import configparser
import io
import json
import pandas as pd

def get_tree_and_samples(complete=False):
    """Function to connect to S3 project bucket to explore content

    This pipeline displays the following information:

    1. AWS credentials store in the 'cfg' file.
    2. The 'song_data' subfolder distribution in 'song_data/A/Z/'.
    3. Some JSON file names stored in 'song_data/A/A/'.
    4. Content of one sampled JSON file from the 'song_data/A/A/' path.
    5. The 'log_data' sub folder distribution in 'log_data/2018/'.
    6. Some JSON file names stored in 'log_data/2018/11/'.
    7. Content of one sampled JSON file from the 'log_data/2018/11/'
       path.
    8. Content of JSON file in S3 from 'log_json_path.json' path.

    Parameters
    ----------
    complete : bool
        description -> Print process workflow and results displaying all
            the paths from S3.
        format -> No apply
        options -> No apply
    """
    config = configparser.ConfigParser()
    config.read_file(open('dwh.cfg'))
    KEY = config.get('AWS', 'KEY')
    SECRET = config.get('AWS', 'SECRET')
    print(f"Print AWS credentials:\n{KEY}\n{SECRET}\n")

    # Get Subfolders of songs
    print("Get songs data insights.\n")
    s3_client = boto3.client(
        's3',
        region_name="us-east-1",
        aws_access_key_id=KEY,
        aws_secret_access_key=SECRET
    )

    objects = s3_client.list_objects(
        Bucket='udacity-dend',
        Prefix='song_data/A/Z/',
        Delimiter='/'
    )

    for object_ in objects.get('CommonPrefixes'):
        print('sub folder : ', object_.get('Prefix'))

    # Show some file names
    if complete:
        s3 = boto3.resource(
            's3',
            region_name="us-east-1",
            aws_access_key_id=KEY,
            aws_secret_access_key=SECRET
        )
        print("\nS3 resource created.\n")

        songs_json_files =  s3.Bucket("udacity-dend")
        for obj in songs_json_files.objects.filter(Prefix="song_data/A/A/A/"):
            print(obj)
        print('')
    
    # Show a single JSON file
    json_file = s3_client.get_object(
        Bucket='udacity-dend',
        Key='song_data/A/A/A/TRAAAYL128F4271A5B.json'
    )
    json_file = json_file["Body"].read().decode()
    json_file = json.loads(json_file)
    print(f"\n{json.dumps(json_file, indent=2, sort_keys=False)}\n")

    # Get Subfolders of log data
    print("\nGet log data insights.\n")
    objects = s3_client.list_objects(
        Bucket='udacity-dend',
        Prefix='log_data/2018/',
        Delimiter='/'
    )

    for object_ in objects.get('CommonPrefixes'):
        print('sub folder : ', object_.get('Prefix'))

    # Get some file names
    if complete:
        print("\nList some files.\n")
        log_json_files =  s3.Bucket("udacity-dend")
        for obj in log_json_files.objects.filter(Prefix="log_data/2018/"):
            print(obj)
    
    # Show a single JSON file
    print("\nShow a sample.\n")
    log_file = s3_client.get_object(
        Bucket='udacity-dend',
        Key='log_data/2018/11/2018-11-18-events.json'
    )
    with io.BytesIO(log_file["Body"].read()) as json_file:
        log_file = pd.read_json(json_file, lines=True)

    print(log_file.head(10))

    # Shows the content of 'log_json_path.json'
    json_file = s3_client.get_object(
        Bucket='udacity-dend',
        Key='log_json_path.json'
    ) 
    json_file = json_file["Body"].read().decode()
    json_file = json.loads(json_file)
    print(f"\n{json.dumps(json_file, indent=2, sort_keys=False)}\n")

def main():
    get_tree_and_samples(complete=True)

if __name__ == "__main__":
    main()
